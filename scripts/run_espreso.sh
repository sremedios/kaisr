#!/usr/bin/bash

IN_FPATH=$1
OUT_DIR=$2
ESPRESO_CONTAINER_FPATH=$3
GPU_ID=$4

# Process ID of input file
IN_FNAME=$(basename "${IN_FPATH}")
IN_ID=${IN_FNAME%.nii*}
IN_EXT=${IN_FNAME#${IN_ID}}
ID_DIR="${OUT_DIR}/${IN_ID}"
mkdir -p ${ID_DIR}

##### ESPRESO CODE #####
ESPRESO_DIR="${ID_DIR}/ESPRESO2_output"
parentdir=$(realpath ${ID_DIR})
SINGULARITYENV_CUDA_VISIBLE_DEVICES=${GPU_ID} singularity run -B $parentdir --nv \
    ${ESPRESO_CONTAINER_FPATH} train.py -i ${IN_FPATH} -o ${ESPRESO_DIR}
KERNEL_FILE="${ESPRESO_DIR}/estimated_filter.npy"
