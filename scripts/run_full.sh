# arguments
IN_FPATH=$1
OUT_DIR=$2
FWHM=$3
GAP=$4
GPU_ID=$5
ESPRESO_CONTAINER_FPATH="//remedios/espreso2/espreso2_031.sif"

# Process ID of input file
IN_FNAME=$(basename "${IN_FPATH}")
IN_ID=${IN_FNAME%.nii*}
IN_EXT=${IN_FNAME#${IN_ID}}
ID_DIR="${OUT_DIR}/${IN_ID}"
EXPR_DIR="${ID_DIR}/${FWHM}x_${GAP}gap"

# Run ESPRESO
#/home/remedios/kaisr/scripts/run_espreso.sh ${IN_FPATH} ${EXPR_DIR} ${ESPRESO_CONTAINER_FPATH} ${GPU_ID}
#ESPRESO_FPATH="${EXPR_DIR}/${IN_ID}/ESPRESO2_output/result.npy"

# On the DGX, we already ran ESPRESO and don't have singularity,
# so we'll just point to the existing one.
res=`printf %02d ${FWHM}`
sep=$(($FWHM + $GAP))
ESPRESO_FPATH="${OUT_DIR%fbreg*}espresso-smore_${res}x_${sep}sep/ESPRESO2_output/result.npy"

# Run stage 1
FILTERS_DIR="${EXPR_DIR}/filters"
python /ISFILE3/USERS/remediossw/kaisr/kaisr/run_stage1.py ${IN_FPATH} ${FILTERS_DIR} ${ESPRESO_FPATH} ${FWHM} ${GAP} ${GPU_ID}

# Run stage 2
python /ISFILE3/USERS/remediossw/kaisr/kaisr/run_stage2.py ${IN_FPATH} ${FILTERS_DIR} ${EXPR_DIR} ${FWHM} ${GAP} ${GPU_ID}
