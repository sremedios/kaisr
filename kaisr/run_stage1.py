from datetime import datetime
import numpy as np
from pathlib import Path
import nibabel as nib
import sys

import torch
import torch.nn as nn
from torch.utils.data import DataLoader

from utils.train_loader_1D_FB import TrainSet as TrainSet1D
from utils.train_loader_1D_FB import normalize
from utils.filter_bank_utils import *
from tqdm import tqdm


class Network(nn.Module):
    def __init__(self, init_codes, features, device):
        super().__init__()

        self.L = len(init_codes[0])
        M = len(init_codes)

        # initial latent codes
        init_codes = [torch.from_numpy(init_code).to(device)
                      for init_code in init_codes]

        self.latent_codes = [torch.zeros_like(init_code, requires_grad=True, device=device)
                             for init_code in init_codes]
        for i in range(M):
            self.latent_codes[i].data = init_codes[i].data

    def forward(self):
        filters = [x for x in self.latent_codes]

        return filters


def modulate(h0, k, M, is_analysis=True):
    n = torch.linspace(0, len(h0)-1, len(h0), device=device)
    C = np.sqrt(2 / M)
    mod = C * torch.cos((k + 1/2) * (n + (M + 1) / 2) * (torch.pi / M))
    return h0 * mod


print("="*20, "STAGE 1", "="*20)

inp_fpath = Path(sys.argv[1])
weight_dir = Path(sys.argv[2])
if not weight_dir.exists():
    weight_dir.mkdir(parents=True)
slice_profile_fpath = Path(sys.argv[3])
fwhm = int(sys.argv[4])
gap = int(sys.argv[5])
M = fwhm + gap
gpu_id = int(sys.argv[6])
device = device = torch.device(f'cuda:{gpu_id}')

# Load up data
batch_size = 2**6
n_items = 50000 * batch_size

ds_stage_1 = TrainSet1D(vol_fpath=inp_fpath, lr_dim=2, M=M, n_items=n_items)
loader_stage_1 = DataLoader(
    ds_stage_1,
    batch_size=batch_size,
    shuffle=True,
    pin_memory=True,
    num_workers=8,
)

# Load h0
h0 = torch.from_numpy(np.load(slice_profile_fpath)).to(device)

# Initialize filters
hs = [h0]
for k in range(1, M):
    h_k = modulate(h0, k, M)
    hs.append(h_k)
hs = [h.detach().cpu().numpy() for h in hs]

init_codes = hs * 2
# discard first filter; we're given h0 and don't need to learn it
init_codes = init_codes[1:]
init_codes = [init_code.astype(np.float32) for init_code in init_codes]

G = Network(init_codes=init_codes, features=len(
    init_codes[0])*256, device=device).to(device)

# Learning params
lr = 1e-1
opt = torch.optim.AdamW(G.latent_codes, lr=lr)

loss_fn = torch.nn.MSELoss().to(device)

scheduler = torch.optim.lr_scheduler.OneCycleLR(
    optimizer=opt,
    max_lr=lr,
    total_steps=n_items+1,
    cycle_momentum=True,
)

opt.step()

with tqdm(total=n_items) as pbar:
    pbar_dict = {
        'loss': np.finfo(np.float32).max,
    }
    for cur_batch, xs_cpu in enumerate(loader_stage_1):
        opt.zero_grad()

        xs = xs_cpu.to(device)

        filters = G()
        hs = [h0, *filters[:M-1]]
        fs = filters[M-1:]

        coefs = analyze(xs, hs)
        xs_hat = synthesize(coefs, fs)

        loss = loss_fn(xs_hat, xs)
        loss.backward()
        opt.step()

        if cur_batch % 5 == 0:
            pbar_dict['loss'] = loss.detach().cpu().numpy().item()
            pbar.set_postfix(
                {k: f'{v:.6f}' for k, v in pbar_dict.items() if v != 0})

        scheduler.step()
        pbar.update(batch_size)

# Save results
torch.save([h.detach().cpu() for h in hs], weight_dir / "hs.pt")
torch.save([f.detach().cpu() for f in fs], weight_dir / "fs.pt")
