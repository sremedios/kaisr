import numpy as np
import torch
import torch.nn.functional as F
from resize.pytorch import resize
from scipy.interpolate import interp1d

from scipy import ndimage
from scipy.signal import windows


WINDOW_OPTIONS = ['blackman', 'hann', 'hamming', 'gaussian', 'cosine', 'parzen']

def select_kernel(window_size, window_choice=None, fwhm=None, sym=True):
    if window_choice is None:
        window_choice = np.random.choice(WINDOW_OPTIONS)
    elif window_choice not in WINDOW_OPTIONS:
        raise ValueError('Window choice (%s) is not supported.' % window_choice)

    window = getattr(windows, window_choice)
    if window_choice in ['gaussian']:
        return window(window_size, fwhm_to_std(fwhm), sym)
    else:
        return window(window_size, sym)

def simulate_lr(vol, kernel, M, lr_axis=2):
    img_slice_batch = torch.moveaxis(vol, lr_axis, 0)
    D = img_slice_batch.shape[1]
    img_slice_1d = batch_2d_to_1d(img_slice_batch)
    lr_slice_1d = batch_downsample_m(apply_filter(img_slice_1d, kernel, padding=False), M)
    lr_slice_2d = batch_1d_to_2d(lr_slice_1d, D)
    return torch.moveaxis(lr_slice_2d, 0, lr_axis)

def synthesize(coefs, fs, divisibility_match=True, filterless=False, apply_crop=False):
    '''
    Execute the synthesis bank given the corresponding
    coefficients and filters.
    
    Overall delay of the system:
    M - 1 + L * M
    What is L? If we let L = 1, this appears to track...
    '''
    M = len(fs)
    L = 1 # TODO: is this a correct assumption?
    delay = M - 1 + L * M
    # The extra cropping needed if the original signal is even and the filter is also even
    # or if the signal is odd and the filter is odd.
    # If these do not match, do not need extra padding
    crop = 1 if divisibility_match else 0
    crop += 2 * (len(fs[0]) - 1) - delay
    crop = int(crop)
    
    if filterless:
        return torch.stack(
            [batch_upsample_m(coef, M) for coef, f in zip(coefs, fs)],
            dim=0).sum(dim=0)
    
    # pad all inputs so vectors are the same length
#     largest_len = np.max([c.shape[1] for c in coefs])
#     pads = [np.abs(largest_len - c.shape[1]) for c in coefs]
#     coefs = [torch.nn.functional.pad(c, (0, p)) for c, p in zip(coefs, pads)]
    
    synthesis = [apply_filter(batch_upsample_m(coef, M), f)
                   for coef, f in zip(coefs, fs)]
    
    # pad the end so each synthesized vector is the same length
#     largest_len = np.max([s.shape[1] for s in synthesis])
#     pads = [np.abs(largest_len - s.shape[1]) for s in synthesis]
#     synthesis = [torch.nn.functional.pad(s, (0, p)) for s, p in zip(synthesis, pads)]
    
    if apply_crop:
        return torch.stack(synthesis, dim=0).sum(dim=0)[:, delay:-crop]
    return torch.stack(synthesis, dim=0).sum(dim=0)[:, delay:]


def analyze(signal, hs, filterless=False, padding=True):
    M = len(hs)
    
    if filterless:
        return [batch_downsample_m(signal, M) for h in hs]
    
    return [batch_downsample_m(apply_filter(signal, h, padding=padding), M) for h in hs]

def apply_filter(signal, filter_, padding=True):
    if padding:
        padding = len(filter_) - 1
    else:
        padding = 'same'
    return F.conv1d(
            signal.unsqueeze(1), 
            filter_.flip(dims=(0,)).unsqueeze(0).unsqueeze(1), 
            stride=1, 
            padding=padding,
        ).squeeze(1)

def batch_upsample_m(signal, M):
    ''' signal is a batch of 1D tensors; M is an integer '''
    B = signal.shape[0]
    L = signal.shape[1]
    
    up = torch.zeros((B, int(M*L)), dtype=signal.dtype, device=signal.device)
    up[:, ::M] = signal
    
#     up = resize(signal.unsqueeze(0).unsqueeze(1), (1, 1/M), order=3).squeeze(1).squeeze(0)
    
    return up

def batch_downsample_m(signal, M):
    # pad the signal to be a multiple of M
    B = signal.shape[0]
    L = signal.shape[1]
    T = int(np.ceil(L / M) * M) - L
    s = F.pad(signal, (0, T))
    
    return s[:, ::M]
#     return resize(signal.unsqueeze(0).unsqueeze(1), (1, M), order=3).squeeze(1).squeeze(0)

def batch_to_vol(signal, D):
    ''' signal is a batch of 1D tensors; BxL '''
    L = signal.shape[1]
    return signal.reshape(D, D, L).numpy()

def vol_to_batch(signal):
    return torch.from_numpy(signal).flatten(start_dim=0, end_dim=1)

def batch_2d_to_1d(signal):
    return signal.flatten(start_dim=0, end_dim=1)

def batch_1d_to_2d(signal, D):
    L = signal.shape[1]
    B = signal.shape[0] // D
    return signal.reshape(B, D, L)

def batch_advance(signal, amt):
    ''' signal is a batch of 1D Torch tensors; BxL '''
    if amt == 0:
        return signal
    return F.pad(signal[:, amt:], (0, amt), mode='constant', value=0.0)

def batch_delay(signal, amt):
    ''' signal is a batch of 1D Torch tensors; BxL '''
    if amt == 0:
        return signal
    return F.pad(signal[:, :-amt], (amt, 0), mode='constant', value=0.0)

def calc_fwhm(kernel):
    """Calculates the full width at half maximum (FWHM) using linear interp.

    Args:
        kernel (numpy.ndarray): The kernel whose FWHM to be calculated.

    Returns
    -------
    fwhm: float
        The calculated FWHM. It is equal to ``right - left``.
    left: float
        The position of the left of the FWHM.
    right: float
        The position of the right of the FWHM.

    """
    kernel = kernel.squeeze()
    half_max = float(np.max(kernel)) / 2
    indices = np.where(kernel > half_max)[0] 
    if len(indices) == 0:
        return 0, 0, 0

    left = indices[0]
    if left > 0:
        interp = interp1d((kernel[left-1], kernel[left]), (left - 1, left))
        left = interp(half_max)
    right = indices[-1]
    if right < len(kernel) - 1:
        interp = interp1d((kernel[right+1], kernel[right]), (right + 1, right))
        right = interp(half_max)
    fwhm = right - left
    return fwhm, left, right