import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from scipy.signal.windows import gaussian

def std_to_fwhm(sigma):
    return 2 * np.sqrt(2 * np.log(2)) * sigma

def fwhm_to_std(gamma):
    return gamma / (2 * np.sqrt(2 * np.log(2)))

def fwhm_needed(fwhm_hr, fwhm_lr):
    return np.sqrt(fwhm_lr ** 2 - fwhm_hr ** 2)

class DegradationKernel(nn.Module):
    def __init__(
        self,
        scale_factor,
        lr_dims,
        blur_kernel,
        device,
        mode='linear', 
        recompute_scale_factor=False,
        align_corners=False,
    ):
        super().__init__()

        self.blur_kernel = blur_kernel / blur_kernel.sum()        
        self.blur_kernel = torch.from_numpy(self.blur_kernel).unsqueeze(0).unsqueeze(1).to(device)
        self.lr_dims = lr_dims
        
        self.scale_factor = scale_factor
        self.mode = mode
        self.recompute_scale_factor = recompute_scale_factor
        self.align_corners = align_corners if self.mode != "nearest" else None
        
    def forward(self, x):
        # if the image is not the correct square shape, resample
        if x.shape != self.lr_dims:
            # First resample to 256 x 256
            x = F.interpolate(
                x, 
                size=self.lr_dims,
                mode='bilinear', 
                recompute_scale_factor=False,
                align_corners=self.align_corners,
            )
        # rearrange; we need to remove the batch dim and treat
        # the first spatial dim as batch
        x = x.squeeze().unsqueeze(1)
        # Blur
        x = F.conv1d(x, self.blur_kernel, padding='same')
        # Subsample
        x = F.interpolate(
            x, 
            scale_factor=self.scale_factor, 
            mode=self.mode, 
            recompute_scale_factor=self.recompute_scale_factor,
            align_corners=self.align_corners,
        )
        # return as spatial
        x = x.squeeze()
        return x