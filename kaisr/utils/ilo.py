from tqdm import tqdm
import torch
import torch.nn as nn
import numpy as np
import lpips



def pred_to_img(G, z, pad, scale):
    with torch.no_grad():
        ret = G(z).detach().cpu().numpy().squeeze()[:, int(pad*scale):]
    return ret

def project_onto_l1_ball(x, eps):
    """
    From ILO code
    
    See: https://gist.github.com/tonyduan/1329998205d88c566588e57e3e2c0c55
    """
    original_shape = x.shape
    x = x.view(x.shape[0], -1)
    mask = (torch.norm(x, p=1, dim=1) < eps).float().unsqueeze(1)
    mu, _ = torch.sort(torch.abs(x), dim=1, descending=True)
    cumsum = torch.cumsum(mu, dim=1)
    arange = torch.arange(1, x.shape[1] + 1, device=x.device)
    rho, _ = torch.max((mu * arange > (cumsum - eps)) * arange, dim=1)
    theta = (cumsum[torch.arange(x.shape[0]), rho.cpu() - 1] - eps) / rho
    proj = (torch.abs(x) - theta.unsqueeze(1)).clamp(min=0)
    x = mask * x + (1 - mask) * proj * torch.sign(x)
    return x.view(original_shape)

def ilo(G, z_init, y, A, lrs, n_iters, N_ILO_LAYERS, R, l1_epsilons, pad, scale, device):
    # track latent vectors in a list
    zs = [z_init.detach()]
    # predict as we go for debugging
    preds = [pred_to_img(G, z_init, pad, scale)]
    
    # set up loss functions
    losses = {
        'mse': torch.nn.MSELoss().to(device),
#         'lpips': lpips.LPIPS(net='vgg').to(device),
#         'tv': TotalVariation().to(device),
    }

    # Decompose generator
    Gs = list(G.children())

    for i in range(1, N_ILO_LAYERS + 1):
        # decompose generator by one layer
        G_curr = Gs[i]
        G_curr_to_img = torch.nn.Sequential(*Gs[i:])
        G_prev = Gs[i-1]
        G_prev_to_img = torch.nn.Sequential(*Gs[i-1:])

        ##### CSGM previous layer #####
        print("#"*10, f"CSGM Layer {i-1}", "#"*10)
        z = torch.zeros_like(zs[i-1], requires_grad=True, device=device)
        z.data = zs[i-1].data
        opt = torch.optim.Adam([z], lr=lrs[i-1])

        with tqdm(total=n_iters[i-1]) as pbar:    
            pbar_dict = {
                'loss': np.finfo(np.float32).max,
                'mse': np.finfo(np.float32).max,
#                 'lpips': np.finfo(np.float32).max,
#                 'tv': np.finfo(np.float32).max,
            }
            for _ in range(n_iters[i-1]):
                opt.zero_grad()

                x_hat = G_prev_to_img(z)[0, 0].unsqueeze(1)
                if A == None:
                    y_hat = x_hat
                else:
                    y_hat = A(x_hat).squeeze()

                mse_loss = losses['mse'](y_hat, y)
#                 lpips_loss = losses['lpips'](y_hat, y)
#                 tv_loss = losses['tv'](x_hat) # smoothness in HR estimate
#                 loss = mse_loss + lpips_loss + tv_loss
                loss = mse_loss

                loss.backward()
                opt.step()

                pbar_dict['loss'] = loss.detach().cpu().numpy().item()
#                 pbar_dict['mse'] = mse_loss.detach().cpu().numpy().item()
#                 pbar_dict['lpips'] = lpips_loss.detach().cpu().numpy().item()
#                 pbar_dict['tv'] = tv_loss.detach().cpu().numpy().item()
                pbar.set_postfix({k: f'{v:.4f}' for k, v in pbar_dict.items()})
                pbar.update(1)

        # get current intermediate representation
        zs.append(G_prev(z).detach())

        ##### L1 ball current layer #####
        print("#"*10, f"L1 Ball Layer {i}", "#"*10)

        for r in range(R):
            # set up optimization for z_i
            z = torch.zeros_like(zs[i], requires_grad=True, device=device)
            z.data = zs[i].data
            opt = torch.optim.Adam([z], lr=lrs[i-1])

            if r > 0:
                # set up previous z_i
                z_i_prev_round = z.clone().detach()

            # optimize as normal, constraint enforced within loop
            with tqdm(total=n_iters[i-1]) as pbar:    
                pbar_dict = {
                    'loss': np.finfo(np.float32).max,
#                     'mse': np.finfo(np.float32).max,
#                     'lpips': np.finfo(np.float32).max,
#                     'tv': np.finfo(np.float32).max,
                }
                for _ in range(n_iters[i-1]):
                    opt.zero_grad()

                    x_hat = G_curr_to_img(z)[0, 0].unsqueeze(1)
                    y_hat = A(x_hat).squeeze()

                    mse_loss = losses['mse'](y_hat, y)
#                     lpips_loss = losses['lpips'](y_hat, y)
#                     tv_loss = losses['tv'](x_hat) # smoothness in HR estimate
#                     loss = mse_loss + lpips_loss# + tv_loss
                    loss = mse_loss

                    loss.backward()
                    opt.step()

                    pbar_dict['loss'] = loss.detach().cpu().numpy().item()
#                     pbar_dict['mse'] = mse_loss.detach().cpu().numpy().item()
#                     pbar_dict['lpips'] = lpips_loss.detach().cpu().numpy().item()
#                     pbar_dict['tv'] = tv_loss.detach().cpu().numpy().item()
                    pbar.set_postfix({k: f'{v:.4f}' for k, v in pbar_dict.items()})

                    # to do Projected Gradient Descent, we enforce our constraint
                    # after each optimizer step but within the loop
                    # So here we project our solution within the L1 ball centered
                    # on the previous solution
                    if r == 0:
                        # first round, just look around the current solution
                        project_onto_l1_ball(zs[i], l1_epsilons[r])
                    else:
                        project_onto_l1_ball(zs[i] - z_i_prev_round, l1_epsilons[r])

                    pbar.update(1)

            # Project back into generator's range by optimizing z_{i-1}
            # to be close to z_i
            z = torch.zeros_like(zs[i-1], requires_grad=True, device=device)
            z.data = zs[i-1].data
            opt = torch.optim.Adam([z], lr=lrs[i-1])

            with tqdm(total=n_iters[i-1]) as pbar:    
                pbar_dict = {'mse': np.finfo(np.float32).max}
                for _ in range(n_iters[i-1]):
                    opt.zero_grad()
                    mse_loss = losses['mse'](G_prev(z), zs[i])
                    mse_loss.backward()
                    opt.step()

                    pbar_dict['mse'] = mse_loss.detach().cpu().numpy().item()
                    pbar.set_postfix({k: f'{v:.4f}' for k, v in pbar_dict.items()})
                    pbar.update(1)

            # update z_i
            zs[i] = G_prev(z).detach()

        preds.append(pred_to_img(G_curr_to_img, zs[i], pad, scale))
    return preds