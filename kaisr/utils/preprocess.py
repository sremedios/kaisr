import time

import numpy as np
import nibabel as nib
from resize.scipy import resize

from .rotate import rotate_vol_2d
from .pad import target_pad
from .degrade import fwhm_needed


def reorient(img, lr_axis):
    """Orient s.t. axes correspond with original axes"""

    if lr_axis == 0:
        return img.transpose(2, 0, 1)
    elif lr_axis == 1:
        return img.transpose(1, 2, 0)
    elif lr_axis == 2:
        return img


def open_interp_rotate(
        img_fpath,
        n_rots=2,
        min_angle=0,
        max_angle=90,
        angles=None,
        dtype=np.float32,
        verbose=True,
        init_interp=True,
        interp_order=5,
        lr_axis=None,
        slice_thickness=None,
        slice_spacing=None,
        acq_dim=2,
):
    if acq_dim != 2:
        raise ValueError('3D Acquisition not currently supported.')

    # Load and preprocess the input file
    if angles is None:
        if n_rots == 1:
            angles = [0]
        else:
            angles = range(min_angle, max_angle + 1, max_angle // (n_rots - 1))

    # Load nifti object
    obj = nib.load(img_fpath)

    # Keep affine and header
    affine = obj.affine
    header = obj.header

    # Get voxel spatial resolution
    acq_res = obj.header.get_zooms()

    # Thickness and downsampling step
    if slice_spacing is None:
        slice_spacing = max(acq_res)
    if slice_thickness is None:
        slice_thickness = slice_spacing
    downsample_ratio = slice_spacing / min(acq_res)
    blur_fwhm = fwhm_needed(min(acq_res), slice_thickness)

    # Load value tensor into RAM
    img = obj.get_fdata(dtype=dtype)

    ########## BEGIN PROCESSING ##########
    if verbose:
        print('{} PREPROCESSING IMAGE {}'.format('=' * 10, '=' * 10))

    ##### Parse the LR axis #####
    lr_axis = int(np.argmax(acq_res)) if lr_axis is None else lr_axis

    ##### Orient s.t. "z" is the LR axis #####
    if lr_axis == 0:
        img = img.transpose(1, 2, 0)
    elif lr_axis == 1:
        img = img.transpose(2, 0, 1)
    # if lr_axis == 2, no change

    ##### Interpolate by default #####
    if init_interp:
        if verbose:
            print('Interpolating through-plane...')
            st = time.time()
        dxyz = (1, 1, 1 / downsample_ratio)
        img = resize(img, dxyz, order=interp_order)
        if verbose:
            en = time.time()
            print('\tElapsed time: {:.4f}s'.format(en - st))

    ##### Pad to square in-plane #####
    in_plane_shape = max(
        img.shape[0],
        img.shape[1],
    )
    target_shape = (in_plane_shape, in_plane_shape, img.shape[2])
    img, pads = target_pad(img, target_shape)

    ##### Rotate #####
    if verbose:
        print('Creating {} rotations...'.format(n_rots))
        st = time.time()
    imgs_rot = [rotate_vol_2d(img, angle) for angle in angles]
    if verbose:
        en = time.time()
        print('\tElapsed time: {:.4f}s'.format(en - st))

    return imgs_rot, affine, header, pads, acq_res, downsample_ratio, blur_fwhm, lr_axis
