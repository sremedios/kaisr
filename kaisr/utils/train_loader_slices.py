import numpy as np
from torch.utils.data import Dataset
import torch
import nibabel as nib
import time
from utils.patch_ops import *

def normalize(x, a=-1, b=1):
    orig_min = x.min()
    orig_max = x.max()

    numer = (x - orig_min) * (b - a)
    denom = orig_max - orig_min

    return a + numer / denom, orig_min, orig_max

class TrainSet(Dataset):
    def __init__(
            self,
            img_fpath,
            n_iters,
            slice_axis,
            augment=True,
            dtype=np.float32,
    ):
        self.n_iters = n_iters
        self.slice_axis = slice_axis
        
        self.vol = nib.load(img_fpath).get_fdata(dtype=dtype)
        self.vol, self.orig_min, self.orig_max = normalize(self.vol)
        self.img_slices = np.rollaxis(self.vol, self.slice_axis)

    def __len__(self):
        return self.n_iters
    
    def get_all_slices_as_tensor(self):
        return torch.from_numpy(self.img_slices).unsqueeze(1)

    def __getitem__(self, i):
        return self.img_slices[i % self.img_slices.shape[0]][np.newaxis, ...]