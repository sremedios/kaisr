import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.parametrizations import spectral_norm
import numpy as np

def pixel_shuffle(x, scale):
    """https://gist.github.com/davidaknowles/6e95a643adaf3960d1648a6b369e9d0b"""
    num_batches, num_channels, nx, ny = x.shape
    num_channels = num_channels // (scale ** 2)
    out = x.contiguous().view(num_batches, num_channels, scale ** 2, nx, ny)
    out = out.permute(0, 1, 3, 2, 4).contiguous()
    out = out.view(num_batches, num_channels, nx * scale, ny * scale)
    return out

class PixelShuffleConv(nn.Module):
    def __init__(self, inp_ch, out_ch, kernel_size, act):
        super().__init__()
        self.kernel_size = kernel_size
        
        self.conv = nn.Conv2d(
            inp_ch, 
            out_ch * kernel_size**2, 
            kernel_size=kernel_size, 
            stride=kernel_size, 
            padding=0,
        )
        self.act = act

    def forward(self, x):
        # dynamic padding based on leftovers
        p1 = self.kernel_size - x.shape[-2] % self.kernel_size
        p2 = self.kernel_size - x.shape[-1] % self.kernel_size
        
        if p2 != self.kernel_size:
            x = F.pad(x, (0, p2, 0, 0), mode='reflect')
        if p1 != self.kernel_size:
            x = F.pad(x, (0, 0, 0, p1), mode='reflect')
        out = self.conv(x)
        out = self.act(out)
        out = pixel_shuffle(out, self.kernel_size)
        # crop off the padding
        if p1 != self.kernel_size:
            out = out[..., :-p2, :]
        if p2 != self.kernel_size:
            out = out[..., :-p1]
        return out

class Sine(nn.Module):
    def __init__(self, omega=30):
        super().__init__()
        self.omega = omega

    def forward(self, input):
        # See paper sec. 3.2, final paragraph, and supplement Sec. 1.5 for discussion of factor 30
        return torch.sin(self.omega * input)

def sine_init(m):
    with torch.no_grad():
        if hasattr(m, 'weight'):
            num_input = m.weight.size(-1)
            # See supplement Sec. 1.5 for discussion of factor 30
            m.weight.uniform_(-np.sqrt(6 / num_input) / 30, np.sqrt(6 / num_input) / 30)

class CALayer(nn.Module):
    def __init__(self, filters, reduction=16):
        super(CALayer, self).__init__()

        # global average pooling: feature --> point
        self.avg_pool = nn.AdaptiveAvgPool2d(1)

        # feature channel downscale and upscale --> channel weight
        self.conv_du = nn.Sequential(
            nn.Conv2d(filters, filters // reduction, 1, padding=0, bias=True),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(filters // reduction, filters, 1, padding=0, bias=True),
            nn.Sigmoid()
        )

    def forward(self, x):
        y = self.avg_pool(x)
        y = self.conv_du(y)
        return x * y
    
class ResBlock(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        res_scale,
        act,
        padding_mode='zeros',
    ):
        super(ResBlock, self).__init__()

        padding = kernel_size // 2
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=self.in_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            act,
            nn.Conv2d(
                in_channels=self.out_channels,
                out_channels=self.out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )
        self.res_scale = res_scale
        self.attn = CALayer(filters=self.out_channels, reduction=16)

    def forward(self, x):
        out = self.body(x).mul(self.res_scale)
        attn = self.attn(x)
        return out + attn


class MultiNet(nn.Module):
    def __init__(self, inp_ch, out_ch, M, L, features, out_patch_size=None, act='lrelu'):
        super().__init__()
        
        if act == 'lrelu':
            act = nn.LeakyReLU(inplace=True)
        elif act == 'relu':
            act = nn.ReLU(inplace=True)
        elif act == 'tanh':
            act = nn.Tanh()
        elif act == 'sin':
            act = Sine()
        else:
            raise ValueError('Invalid activation string argument')
            
        self.out_patch_size = out_patch_size
            
        self.heads = nn.ModuleList([
            nn.Sequential(
                nn.Conv2d(inp_ch, features, kernel_size=3, stride=1, padding='same'),
                act,
            ) for _ in range(M-1)
        ])
        
        self.bodies = nn.ModuleList([
            nn.Sequential(
                *[
                    ResBlock(
                        in_channels=features,
                        out_channels=features,
                        kernel_size=3,
                        res_scale=0.1,
                        act=act,
                    ) for _ in range(L)
                ]
            ) for _ in range(M-1)
        ])
        
        self.attns = nn.ModuleList([CALayer(filters=features, reduction=4)
                                    for _ in range(M-1)])
        
        self.tails = nn.ModuleList([
            nn.Sequential(
                nn.Conv2d(features, features, kernel_size=3, stride=1, padding='same'),
                nn.LeakyReLU(inplace=True),
                nn.Conv2d(features, out_ch, kernel_size=1, stride=1, padding='same'),
            ) for _ in range(M-1)
        ])
        
        self.pool = nn.AdaptiveAvgPool2d(out_patch_size)

    def forward(self, x):
        head_ds = [head(x) for head in self.heads]
        body_ds = [body(d) for body, d in zip(self.bodies, head_ds)]
        attn_ds = [attn(h+b) for h, b, attn in zip(head_ds, body_ds, self.attns)]
        tail_ds = [tail(a).squeeze() for a, tail in zip(body_ds, self.tails)]
        if self.out_patch_size is not None:
            tail_ds = [self.pool(t) for t in tail_ds]
        return torch.stack(tail_ds, dim=0)
    
class SmallNet(nn.Module):
    def __init__(self, inp_ch, out_ch, kernel_size, M, L, features, act='lrelu'):
        super().__init__()
        
        if act == 'lrelu':
            act = nn.LeakyReLU(inplace=True)
        elif act == 'relu':
            act = nn.ReLU(inplace=True)
        elif act == 'tanh':
            act = nn.Tanh()
        elif act == 'sin':
            act = Sine()
        else:
            raise ValueError('Invalid activation string argument')
            
        self.heads = nn.ModuleList([
            PixelShuffleConv(
                inp_ch=inp_ch, 
                out_ch=features, 
                kernel_size=kernel_size, 
                act=act,
            ) for _ in range(M-1)
        ])
        
        self.bodies = nn.ModuleList([
            nn.Sequential(
                *[
                    PixelShuffleConv(
                        inp_ch=features, 
                        out_ch=features, 
                        kernel_size=kernel_size, 
                        act=act,
                    ) for _ in range(L)
                ]
            ) for _ in range(M-1)
        ])
        
        
        self.tails = nn.ModuleList([
            nn.Conv2d(features, out_ch, kernel_size=1, stride=1, padding=0)
            for _ in range(M-1)
        ])

    def forward(self, x):
        head_ds = [head(x) for head in self.heads]
        body_ds = [body(d) for body, d in zip(self.bodies, head_ds)]
        tail_ds = [tail(b).squeeze() for b, tail in zip(body_ds, self.tails)]
        return torch.stack(tail_ds, dim=0)
    
class Network(nn.Module):
    def __init__(self, inp_ch, out_ch, M, L, features, out_patch_size=None, act='lrelu'):
        super().__init__()
        
        if act == 'lrelu':
            act = nn.LeakyReLU(inplace=True)
        elif act == 'relu':
            act = nn.ReLU(inplace=True)
        elif act == 'tanh':
            act = nn.Tanh()
        elif act == 'sin':
            act = Sine()
        else:
            raise ValueError('Invalid activation string argument')
            
        self.out_patch_size = out_patch_size
        
        self.head = nn.Sequential(
                nn.Conv2d(inp_ch, features, kernel_size=3, stride=1, padding='same'),
                act,
            )
        
        self.body = nn.Sequential(
                *[
                    ResBlock(
                        in_channels=features,
                        out_channels=features,
                        kernel_size=3,
                        res_scale=0.1,
                        act=act,
                    ) for _ in range(L)
                ]
            )
        
        self.attn = CALayer(filters=features, reduction=16)
        
        self.tail = nn.Sequential(
                nn.Conv2d(features, features, kernel_size=3, stride=1, padding='same'),
                nn.LeakyReLU(inplace=True),
                nn.Conv2d(features, out_ch, kernel_size=1, stride=1, padding='same'),
            )
        
        self.pool = nn.AdaptiveAvgPool2d(out_patch_size)

    def forward(self, x):
        h = self.head(x)
        b = self.body(h)
        a = self.attn(h+b)
        tail = self.tail(a)
        if self.out_patch_size is not None:
            return self.pool(tail)
        return tail
    
# class Discriminator(nn.Module):
#     def __init__(self, in_ch, out_ch, features, d_patch_size):
#         super().__init__()
        
#         self.body = nn.Sequential(
#             spectral_norm(nn.Conv2d(in_ch, features, kernel_size=3, stride=1, padding='same')),
#             nn.LeakyReLU(inplace=True),
#             spectral_norm(nn.Conv2d(features, features, kernel_size=3, stride=1, padding='same')),
#             nn.LeakyReLU(inplace=True),
#             spectral_norm(nn.Conv2d(features, out_ch, kernel_size=3, stride=1, padding='same')),
#             nn.AdaptiveAvgPool2d(d_patch_size),
# #             nn.Sigmoid(), # for mixed precision training, this is rolled into the loss
#         )

#     def forward(self, x):
#         return self.body(x)