""" Full assembly of the parts to form the complete network """

'''
Modified from Milesial github
'''

import torch.nn.functional as F
from .unet_parts import *
class UNet(nn.Module):
    def __init__(
        self,
        in_ch,
        out_ch,
        filter_ds=1,
        out_patch_size=None,
        bilinear=True,
    ):
        super(UNet, self).__init__()
        
        self.out_patch_size = out_patch_size
        self.bilinear = bilinear

        self.inc = DoubleConv(in_ch, 128//filter_ds)
        self.down1 = Down(128//filter_ds, 256//filter_ds)
        self.down2 = Down(256//filter_ds, 512//filter_ds)
        self.down3 = Down(512//filter_ds, 1024//filter_ds)
        factor = 2 if bilinear else 1
        self.down4 = Down(1024//filter_ds, 2048//filter_ds // factor)
        self.up1 = Up(2048//filter_ds, 1024//filter_ds // factor, bilinear)
        self.up2 = Up(1024//filter_ds, 512//filter_ds // factor, bilinear)
        self.up3 = Up(512//filter_ds, 256//filter_ds // factor, bilinear)
        self.up4 = Up(256//filter_ds, 128//filter_ds, bilinear)
        self.outc = OutConv(128//filter_ds, out_ch)
        
        if out_patch_size is not None:
            self.pool = nn.AdaptiveAvgPool2d(self.out_patch_size)


    def forward(self, x):
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x = self.up1(x5, x4)
        x = self.up2(x, x3)
        x = self.up3(x, x2)
        x = self.up4(x, x1)
        logits = self.outc(x)
        
        if self.out_patch_size is not None:
            logits = self.pool(logits)

        return logits