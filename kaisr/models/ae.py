import torch
from torch import nn
from torch.nn import functional as F
import numpy as np

class Downsample(nn.Module):
    def __init__(self, scale_factor, mode='bilinear', recompute_scale_factor=False, align_corners=False):
        super().__init__()
        
        self.scale_factor = scale_factor
        self.mode = mode
        self.recompute_scale_factor = recompute_scale_factor
        self.align_corners = align_corners
        
    def forward(self, x):
        return F.interpolate(
            x, 
            scale_factor=self.scale_factor, 
            mode=self.mode, 
            recompute_scale_factor=self.recompute_scale_factor,
            align_corners=self.align_corners
        )

class ConvReLU(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size):
        super().__init__()

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding='same',
                bias=True,
            ),
            nn.BatchNorm2d(out_channels),
            nn.Tanh(),
        )

    def forward(self, x):
        return self.body(x)


class ConvReLUDown(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size):
        super().__init__()

        self.body = nn.Sequential(
            Downsample(scale_factor=0.5),
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding='same',
                bias=True,
            ),
            nn.BatchNorm2d(out_channels),
            nn.Tanh(),
        )

    def forward(self, x):
        return self.body(x)


class ConvReLUUp(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size):
        super().__init__()

        self.body = nn.Sequential(
            nn.Upsample(scale_factor=2, mode='bilinear', align_corners=False),
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding='same',
                bias=True,
            ),
            nn.BatchNorm2d(out_channels),
            nn.Tanh(),
        )

    def forward(self, x):
        return self.body(x)


class AE(nn.Module):
    def __init__(self, in_channels, filters, network_depth):
        super(AE, self).__init__()

        max_filters = filters
        enc_filter_list = [max_filters // 2**i for i in range(network_depth + 1)]
        enc_filter_list.reverse()
        next_enc_filter_list = enc_filter_list[1:]
        enc_filter_list = enc_filter_list[:-1]

        dec_filter_list = [max_filters // 2**i for i in range(network_depth + 1)]
        prev_dec_filter_list = dec_filter_list[:-1]
        dec_filter_list = dec_filter_list[1:]

        self.head = ConvReLU(
            in_channels=in_channels,
            out_channels=enc_filter_list[0],
            kernel_size=3,
        )

        self.encoder = nn.Sequential(
            *[
                ConvReLUDown(
                    in_channels=enc_filter_list[i],
                    out_channels=next_enc_filter_list[i],
                    kernel_size=3,
                ) for i in range(network_depth)
            ]
        )

        self.decoder = nn.Sequential(
            *[
                ConvReLUUp(
                    in_channels=prev_dec_filter_list[i],
                    out_channels=dec_filter_list[i],
                    kernel_size=3,
                ) for i in range(network_depth)
            ]
        )

        self.tail = ConvReLU(
            in_channels=dec_filter_list[-1],
            out_channels=in_channels,
            kernel_size=3,
        )

    def forward(self, x):
        return self.tail(self.decoder(self.encoder(self.head(x))))
