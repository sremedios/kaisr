import numpy as np
from torch import nn
import torch
import torch.nn.functional as F
import scipy
import scipy.signal

def design_lowpass_filter(numtaps, cutoff, width, fs, radial=False):
    assert numtaps >= 1

    # Identity filter.
    if numtaps == 1:
        return None

    # Separable Kaiser low-pass filter.
    if not radial:
        f = scipy.signal.firwin(numtaps=numtaps, cutoff=cutoff, width=width, fs=fs)
        f = np.outer(f, f)
        return torch.as_tensor(f, dtype=torch.float32)

    # Radially symmetric jinc-based filter.
    x = (np.arange(numtaps) - (numtaps - 1) / 2) / fs
    r = np.hypot(*np.meshgrid(x, x))
    f = scipy.special.j1(2 * cutoff * (np.pi * r)) / (np.pi * r)
    beta = scipy.signal.kaiser_beta(scipy.signal.kaiser_atten(numtaps, width / (fs / 2)))
    w = np.kaiser(numtaps, beta)
    f *= np.outer(w, w)
    f /= np.sum(f)
    return torch.as_tensor(f, dtype=torch.float32)


class AliasFreeNonLinearity(nn.Module):
    '''
    Inefficient version of the alias-free non-linearity introduced in StyleGAN3.
    In their work, they implement a custom CUDA kernel for speed.
    
    Alternatively, we could possibly figure out a polyphase matrix implementation
    which would also be fast; this is for future work.
    
    TODO: Use existing code from StyleGAN3 to improve speed
    '''
    def __init__(
        self, 
        n_channels, 
        in_cutoff, 
        out_cutoff,
        in_half_width,
        out_half_width,
        tmp_sampling_rate,
        nonlinearity,
        device,
    ):
        super().__init__()

        filter_size = 6 # hyperparam from StyleGAN3
        self.in_cutoff = in_cutoff
        self.out_cutoff = out_cutoff
        self.in_half_width = in_half_width
        self.out_half_width = out_half_width
        self.tmp_sampling_rate = tmp_sampling_rate
        
        # Design upsampling filter.
        self.up_taps = filter_size * 2 + 1
        self.up_filter = design_lowpass_filter(
            numtaps=self.up_taps, 
            cutoff=self.in_cutoff, 
            width=self.in_half_width*2, 
            fs=self.tmp_sampling_rate,
        )
        self.up_filter = self.up_filter\
            .view(1, 1, self.up_filter.shape[0], self.up_filter.shape[1])\
            .repeat(n_channels, n_channels, 1, 1)\
            .to(device)
        
        # Design downsampling filter.
        self.down_taps = filter_size * 0.5 + 1
        self.down_filter = design_lowpass_filter(
            numtaps=self.down_taps, 
            cutoff=self.out_cutoff, 
            width=self.out_half_width*2, 
            fs=self.tmp_sampling_rate,
        )
        self.down_filter = self.down_filter\
            .view(1, 1, self.down_filter.shape[0], self.down_filter.shape[1])\
            .repeat(n_channels, n_channels, 1, 1)\
            .to(device)
        
        self.nonlinearity = nonlinearity
        

    def forward(self, x):
        # upsample
        x = F.interpolate(x, scale_factor=2, mode='nearest', recompute_scale_factor=False)
        # filter
        x = F.conv2d(x, self.up_filter, padding='same')
        # nonlin
        x = self.nonlinearity(x)
        # filter
        x = F.conv2d(x, self.down_filter, padding='same')
        # downsample
        x = F.interpolate(x, scale_factor=0.5, mode='nearest', recompute_scale_factor=False)
        return x
    
    
"""
class AliasFreeNonLinearity(nn.Module):
    '''
    Inefficient version of the alias-free non-linearity introduced in StyleGAN3.
    In their work, they implement a custom CUDA kernel for speed.
    
    Alternatively, we could possibly figure out a polyphase matrix implementation
    which would also be fast; this is for future work.
    
    TODO: Use existing code from StyleGAN3 to improve speed
    '''
    def __init__(
        self, 
        n_channels, 
        in_cutoff, 
        out_cutoff,
        in_half_width,
        out_half_width,
        tmp_sampling_rate,
        nonlinearity,
        device,
    ):
        super().__init__()

        filter_size = 6 # hyperparam from StyleGAN3
        self.in_cutoff = in_cutoff
        self.out_cutoff = out_cutoff
        self.in_half_width = in_half_width
        self.out_half_width = out_half_width
        self.tmp_sampling_rate = tmp_sampling_rate
        
        # Design upsampling filter.
        self.up_taps = filter_size * 2 + 1
        self.up_filter = design_lowpass_filter(
            numtaps=self.up_taps, 
            cutoff=self.in_cutoff, 
            width=self.in_half_width*2, 
            fs=self.tmp_sampling_rate,
        )
        self.up_filter = self.up_filter\
            .view(1, 1, self.up_filter.shape[0], self.up_filter.shape[1])\
            .repeat(n_channels, n_channels, 1, 1)\
            .to(device)
        
        # Design downsampling filter.
        self.down_taps = filter_size * 0.5 + 1
        self.down_filter = design_lowpass_filter(
            numtaps=self.up_taps, 
            cutoff=self.out_cutoff, 
            width=self.out_half_width*2, 
            fs=self.tmp_sampling_rate,
        )
        self.down_filter = self.down_filter\
            .view(1, 1, self.down_filter.shape[0], self.down_filter.shape[1])\
            .repeat(n_channels, n_channels, 1, 1)\
            .to(device)
        
        self.nonlinearity = nonlinearity
        

    def forward(self, x):
        orig_dims = x.shape[-2:]
        # upsample
        x = F.interpolate(x, scale_factor=2, mode='nearest', recompute_scale_factor=False)
        # filter
        x = F.conv2d(x, self.up_filter, padding='same')
        # nonlin
        x = self.nonlinearity(x)
        # filter
        x = F.conv2d(x, self.down_filter, padding='same')
        # downsample
        x = F.interpolate(x, scale_factor=0.5, mode='nearest', recompute_scale_factor=False)
        return x
"""

class UpsamplingBlock(nn.Module):
    def __init__(
            self,
            in_channels,
            out_channels,
            in_cutoff, 
            out_cutoff,
            in_half_width,
            out_half_width,
            in_sampling_rate,
            out_sampling_rate,
            kernel_size,
            padding_mode,
            device,
            is_toimg=False,
            upsample=True,
    ):
        super().__init__()

        padding = kernel_size // 2
        tmp_sampling_rate = max(in_sampling_rate, out_sampling_rate) * (2 if not is_toimg else 1)

        # align_corners preserves the signal value at the corner,
        # which we definitely do NOT want because we want to resample the 
        # entire signal
        scale_factor = 2 if upsample else 1
        self.head = nn.Upsample(scale_factor=scale_factor, mode='bilinear', align_corners=False)
        
        # Body based on insights from StyleGAN3
        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            AliasFreeNonLinearity(
                n_channels=out_channels, 
                in_cutoff=in_cutoff, 
                out_cutoff=out_cutoff,
                in_half_width=in_half_width,
                out_half_width=out_half_width,
                tmp_sampling_rate=tmp_sampling_rate,
                nonlinearity=torch.nn.LeakyReLU(),
                device=device,
            ),
        )

    def forward(self, x):
        return self.body(self.head(x))

class Generator(nn.Module):
    '''
    Simple generator mapping a Z-length noise vector to an N x N x C image
    Adapted from StyleGAN3
    '''
    def __init__(
            self,
            img_resolution, # int, we're generating a square image. Must be a power of 2
            img_channels, # int
            batch_size,
            device,
            kernel_size=3,
            z_dim=256, # int, dimensionalty of z noise
            w_dim=256,
            padding_mode='zeros',
            init_size=4,
            channel_base = 32768,    # Overall multiplier for the number of channels.
            channel_max = 512,      # Maximum number of channels in any layer.
            num_critical = 2,        # Number of critically sampled layers at the end.
            first_cutoff = 2,        # Cutoff frequency of the first layer (f_{c,0}).
            first_stopband = 2**2.1,   # Minimum stopband of the first layer (f_{t,0}).
            last_stopband_rel = 2**0.3,   # Minimum stopband of the last layer, expressed relative to the cutoff.
    ):
        super().__init__()
        padding = kernel_size // 2
        
        self.batch_size = batch_size
        self.margin_size = 0 # TODO: understand this
        self.img_resolution = img_resolution
        self.img_channels = img_channels
        self.init_size = init_size
        self.z_dim = z_dim
        self.w_dim = w_dim
        self.num_critical = num_critical
        self.num_layers = int(np.log2(self.img_resolution / self.init_size)) * 2
        
        # Geometric progression of layer cutoffs and min. stopbands.
        last_cutoff = self.img_resolution / 2 # f_{c,N}
        last_stopband = last_cutoff * last_stopband_rel # f_{t,N}
        exponents = np.minimum(np.arange(self.num_layers + 1) / (self.num_layers - self.num_critical), 1)
        cutoffs = first_cutoff * (last_cutoff / first_cutoff) ** exponents # f_c[i]
        stopbands = first_stopband * (last_stopband / first_stopband) ** exponents # f_t[i]

        # Compute remaining layer parameters.
        sampling_rates = np.exp2(np.ceil(np.log2(np.minimum(stopbands * 2, self.img_resolution)))) # s[i]
        half_widths = np.maximum(stopbands, sampling_rates / 2) - cutoffs # f_h[i]
        sizes = sampling_rates + self.margin_size * 2
        sizes[-2:] = self.img_resolution
        channels = np.rint(np.minimum((channel_base / 2) / cutoffs, channel_max))
        channels[-1] = self.img_channels
        channels = [int(x) for x in channels]
        self.channels = channels
                
        in_filters = channels[:-1]
        out_filters = channels[1:]
        in_cutoffs = cutoffs[:-1]
        out_cutoffs = cutoffs[1:]
        in_half_widths = half_widths[:-1]
        out_half_widths = half_widths[1:]
        in_sampling_rates = sampling_rates[:-1]
        out_sampling_rates = sampling_rates[1:]
                
        # Map noise into a shape
        self.head = nn.Sequential(
            nn.Linear(self.z_dim, channels[0] * self.init_size ** 2),
            nn.LeakyReLU(),
        )

        # processing blocks
        self.body = nn.Sequential(
            *[
                UpsamplingBlock(
                    in_channels=in_filter,
                    out_channels=out_filter,
                    in_cutoff=in_cutoff, 
                    out_cutoff=out_cutoff,
                    in_half_width=in_half_width,
                    out_half_width=out_half_width,
                    in_sampling_rate=in_sampling_rate,
                    out_sampling_rate=out_sampling_rate,
                    kernel_size=kernel_size,
                    padding_mode=padding_mode,
                    is_toimg=(idx == self.num_layers),
                    device=device,
                    upsample=(idx % 2 == 0),
                ) for idx, (in_filter, out_filter, in_cutoff, out_cutoff, in_half_width, 
                        out_half_width, in_sampling_rate, out_sampling_rate) in enumerate(zip(
                            in_filters,
                            out_filters,
                            in_cutoffs,
                            out_cutoffs,
                            in_half_widths,
                            out_half_widths,
                            in_sampling_rates,
                            out_sampling_rates,
                        ))
            ]
        )
        

    
    def forward(self, z):
#         w = self.mapping(z)
        # map noise
        x = self.head(z)
        x = x.view(self.batch_size, self.channels[0], self.init_size, self.init_size)        
        # map up to image
        x = self.body(x)
        # tanh it
        x = nn.Tanh()(x)

        return x
    
class ResBlock(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        res_scale,
    ):
        super(ResBlock, self).__init__()

        self.res_scale = res_scale
        self.kernel_size = kernel_size
        padding = self.kernel_size // 2
        
        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                bias=True,
                padding=padding,
            ),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                bias=True,
                padding=padding,
            ),
        )

    def forward(self, x):
        out = self.body(x).mul(self.res_scale)
        return out + x
    
    
class Discriminator(nn.Module):
    def __init__(self, img_size, channels):
        super(Discriminator, self).__init__()

        def discriminator_block(in_filters, out_filters, bn=True):
            block = [
                nn.Conv2d(in_filters, out_filters, kernel_size=3, stride=2, padding=1), 
                nn.LeakyReLU(0.2, inplace=True), 
                nn.Dropout2d(0.25),
            ]
            if bn:
                block.append(nn.BatchNorm2d(out_filters, 0.8))
            return block

        self.model = nn.Sequential(
            *discriminator_block(channels, 16, bn=False),
            *discriminator_block(16, 32),
            *discriminator_block(32, 64),
            *discriminator_block(64, 128),
        )

        # The height and width of downsampled image
        ds_size = img_size // 2 ** 4
        self.adv_layer = nn.Sequential(nn.Linear(128 * ds_size ** 2, 1), nn.Sigmoid())

    def forward(self, img):
        out = self.model(img)
        out = out.view(out.shape[0], -1)
        validity = self.adv_layer(out)

        return validity