import torch.nn as nn
import torch.nn.functional as F
import torch

class Downsample(nn.Module):
    def __init__(self, scale_factor, mode, recompute_scale_factor, align_corners):
        super().__init__()
        
        self.scale_factor = scale_factor
        self.mode = mode
        self.recompute_scale_factor = recompute_scale_factor
        self.align_corners = align_corners
        
    def forward(self, x):
        return F.interpolate(
            x, 
            scale_factor=self.scale_factor, 
            mode=self.mode, 
            recompute_scale_factor=self.recompute_scale_factor,
            align_corners=self.align_corners
        )

def weights_init_normal(m):
    classname = m.__class__.__name__
    if classname.find("Conv") != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find("BatchNorm2d") != -1:
        torch.nn.init.normal_(m.weight.data, 1.0, 0.02)
        torch.nn.init.constant_(m.bias.data, 0.0)


class Generator(nn.Module):
    def __init__(self, img_size, latent_dim, channels, filters):
        super().__init__()
        
        self.filters = filters
        self.init_size = img_size // 4
        self.l1 = nn.Sequential(
            nn.Linear(latent_dim, filters * self.init_size ** 2),
            nn.LeakyReLU(inplace=True),
        )

        self.conv_blocks = nn.Sequential(
            nn.BatchNorm2d(filters),
            nn.Upsample(scale_factor=2, mode='bilinear', align_corners=False),
            nn.Conv2d(filters, filters//2, 3, stride=1, padding=1),
            nn.BatchNorm2d(filters//2, 0.8),
            nn.LeakyReLU(inplace=True),
            nn.Upsample(scale_factor=2, mode='bilinear', align_corners=False),
            nn.Conv2d(filters//2, filters//4, 3, stride=1, padding=1),
            nn.BatchNorm2d(filters//4, 0.8),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(filters//4, filters//8, 3, stride=1, padding=1),
            nn.LeakyReLU(inplace=True),
            nn.BatchNorm2d(filters//8, 0.8),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(filters//8, channels, 3, stride=1, padding=1),
            nn.Tanh(),
        )

    def forward(self, z):
        out = self.l1(z)
        out = out.view(out.shape[0], self.filters, self.init_size, self.init_size)
        img = self.conv_blocks(out)
        return img


class Discriminator(nn.Module):
    def __init__(self, img_size, channels):
        super().__init__()

        def discriminator_block(in_filters, out_filters, bn=True):
            block = [
                Downsample(scale_factor=0.5, mode='bilinear', 
                           recompute_scale_factor=False, align_corners=False),
                nn.Conv2d(in_filters, out_filters, 3, 1, 1), 
                nn.LeakyReLU(0.2, inplace=True), 
                nn.Dropout2d(0.25),
            ]
            if bn:
                block.append(nn.BatchNorm2d(out_filters, 0.8))
            return block

        self.model = nn.Sequential(
            *discriminator_block(channels, 16, bn=False),
            *discriminator_block(16, 32),
            *discriminator_block(32, 64),
            *discriminator_block(64, 128),
        )

        # The height and width of downsampled image
        ds_size = img_size // 2 ** 4
        self.adv_layer = nn.Linear(128 * ds_size ** 2, 1)
        # NOTE: sigmoid is missing; included in the loss function

    def forward(self, img):
        out = self.model(img)
        out = out.view(out.shape[0], -1)
        validity = self.adv_layer(out)

        return validity