from .base import *
from .vanilla_vae import *
from .iwae import *

vae_models = {
  'IWAE':IWAE,
  'VanillaVAE':VanillaVAE,
}