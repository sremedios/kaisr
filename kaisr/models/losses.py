from kornia.filters import Sobel, Laplacian
import torch
from torch import nn

def laplacian_loss(y_true, y_pred):
    edge_detector = Laplacian(3)
    return torch.mean(torch.abs(edge_detector(y_pred) - edge_detector(y_true)))

def sobel_loss(y_true, y_pred):
    edge_detector = Sobel()
    return torch.mean(torch.abs(edge_detector(y_pred) - edge_detector(y_true)))

def l1_edge_loss(y_true, y_pred):
#     sobel = Sobel()
    edge_detector = Laplacian(3)
    l1 = nn.L1Loss(reduction='mean')

    edge_loss = torch.mean(torch.abs(edge_detector(y_pred) - edge_detector(y_true)))
    l1_loss = l1(y_pred, y_true)

    return edge_loss + l1_loss


def l2_sobel_loss(y_true, y_pred):
    sobel = Sobel()
    l2 = nn.MSELoss(reduction='mean')

    sobel_loss = torch.mean(torch.abs(sobel(y_pred) - sobel(y_true)))
    l2_loss = l2(y_pred, y_true)

    return sobel_loss + l2_loss
